<?php declare(strict_types=1);

namespace App\Tests\Functional\API\Product;

use App\Tests\Functional\API\DbWebTestCase;

class ProductControllerTest extends DbWebTestCase
{
    public const CREATE_PRODUCT = '/api/product';

    public function testGet()
    {
        $this->client->request('GET', self::CREATE_PRODUCT);

        self::assertEquals(405, $this->client->getResponse()->getStatusCode());
    }

    public function testPostError()
    {
        $this->client->request('POST', self::CREATE_PRODUCT, [], [], [],
            json_encode(ProductDataFixture::productDataError())
        );

        self::assertEquals(400, $this->client->getResponse()->getStatusCode());
        self::assertJson($content = $this->client->getResponse()->getContent());

        $data = json_decode($content, true);

        self::assertArraySubset([
            'error' => [
                'message' => [
                    'detail' => 'price: Price must be between 1 and 100'
                ],
            ],
        ], $data);
    }

    public function testPostSuccess()
    {
        $this->client->request('POST', self::CREATE_PRODUCT, [], [], [],
            json_encode(ProductDataFixture::productDataSuccess())
        );

        self::assertEquals(201, $this->client->getResponse()->getStatusCode());
        self::assertJson($content = $this->client->getResponse()->getContent());

        $data = json_decode($content, true);

        self::assertArraySubset([
            'name' => 'Product name',
            'price' => 50
        ], $data);
    }

    public function testPostUniqueException()
    {
        $this->client->request('POST', self::CREATE_PRODUCT, [], [], [],
            json_encode(ProductDataFixture::productDataSuccess())
        );

        self::assertEquals(201, $this->client->getResponse()->getStatusCode());
        self::assertJson($content = $this->client->getResponse()->getContent());

        $data = json_decode($content, true);

        self::assertArraySubset([
            'name' => 'Product name',
            'price' => 50
        ], $data);

        $this->client->request('POST', self::CREATE_PRODUCT, [], [], [],
            json_encode(ProductDataFixture::productDataSuccess())
        );

        self::assertEquals(400, $this->client->getResponse()->getStatusCode());
        self::assertJson($content = $this->client->getResponse()->getContent());

        $data = json_decode($content, true);

        self::assertArraySubset([
            'error' => [
                'message' => [
                    'detail' => 'name: This value is already used.'
                ],
            ],
        ], $data);
    }
}