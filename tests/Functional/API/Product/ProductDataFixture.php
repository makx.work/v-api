<?php declare(strict_types=1);

namespace App\Tests\Functional\API\Product;

class ProductDataFixture
{
    public static function productDataError()
    {
        return [
            'name' => 'Product name',
            'price' => -10
        ];
    }

    public static function productDataSuccess()
    {
        return [
            'name' => 'Product name',
            'price' => 50
        ];
    }
}