<?php declare(strict_types=1);

namespace App\Tests\Functional\API\Health;

use App\Tests\Functional\API\DbWebTestCase;

class HealthControllerTest extends DbWebTestCase
{
    public const HEALTH = '/api/health';

    public function testGet()
    {
        $this->client->request('GET', self::HEALTH);

        self::assertEquals(200, $this->client->getResponse()->getStatusCode());

        $content = $this->client->getResponse()->getContent();
        $data = json_decode($content, true);

        self::assertEquals('OK', $data);
    }
}
