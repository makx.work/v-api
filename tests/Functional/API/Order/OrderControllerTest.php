<?php declare(strict_types=1);

namespace App\Tests\Functional\API\Order;

use App\Tests\Functional\API\DbWebTestCase;
use Liip\TestFixturesBundle\Test\FixturesTrait;
use App\Tests\Functional\Fixtures\OrderFixture;
use App\Tests\Functional\Fixtures\ProductFixture;

class OrderControllerTest extends DbWebTestCase
{
    use FixturesTrait;

    public const CREATE_ORDER = '/api/order';
    public const ORDER_PRODUCT_ADD = '/api/order/product/add';

    public function testGet()
    {
        $this->client->request('GET', self::CREATE_ORDER);

        self::assertEquals(405, $this->client->getResponse()->getStatusCode());
    }

    public function testPostSuccessCreateOrder()
    {
        $this->client->request('POST', self::CREATE_ORDER, [], [], []);

        self::assertEquals(201, $this->client->getResponse()->getStatusCode());
        self::assertJson($content = $this->client->getResponse()->getContent());

        $data = json_decode($content, true);

        self::assertArraySubset([
            'total' => 0
        ], $data);
    }

    public function testPostCreateExceptionNotFound()
    {
        $this->client->request('POST', self::ORDER_PRODUCT_ADD, [], [], [],
            json_encode([
                'productId' => 999,
                'orderId' => 999
            ])
        );

        self::assertEquals(400, $this->client->getResponse()->getStatusCode());
        self::assertJson($content = $this->client->getResponse()->getContent());

        $data = json_decode($content, true);

        self::assertArraySubset([
            'error' => [
                'message' => [
                    'Product or Order not found'
                ],
            ],
        ], $data);
    }

    public function testPostSuccessCreateOrderProductAdd()
    {
        $fixtures = $this->loadFixtures([
            'App\Tests\Functional\Fixtures\OrderFixture',
            'App\Tests\Functional\Fixtures\ProductFixture'
        ])->getReferenceRepository();

        $product = $fixtures->getReference(ProductFixture::REFERENCE_PRODUCT);
        $order = $fixtures->getReference(OrderFixture::REFERENCE_ORDER);

        $this->client->request('POST', self::ORDER_PRODUCT_ADD, [], [], [],
            json_encode([
                'productId' => $product->getId(),
                'orderId' => $order->getId()
            ])
        );

        self::assertEquals(201, $this->client->getResponse()->getStatusCode());
        self::assertJson($content = $this->client->getResponse()->getContent());

        $data = json_decode($content, true);

        self::assertArraySubset([
            'total_price' => $product->getPrice(),
            'products' => [
                [
                    'id' => $product->getId(),
                    'quantity' => 1
                ]
            ]
        ], $data);
    }
}