<?php declare(strict_types=1);

namespace App\Tests\Functional\Fixtures;

use App\Domain\Product\Product;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Persistence\ObjectManager;

class ProductFixture extends Fixture implements FixtureInterface
{
    public const REFERENCE_PRODUCT = 'test_product';

    public function load(ObjectManager $manager)
    {
        $product = new Product('Product', 11);
        $manager->persist($product);

        $this->setReference(self::REFERENCE_PRODUCT, $product);

        $manager->flush();
    }
}
