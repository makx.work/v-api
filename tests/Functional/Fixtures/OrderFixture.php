<?php declare(strict_types=1);

namespace App\Tests\Functional\Fixtures;

use App\Domain\Order\Order;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Persistence\ObjectManager;

class OrderFixture extends Fixture implements FixtureInterface
{
    public const REFERENCE_ORDER = 'test_product';

    public function load(ObjectManager $manager)
    {
        $order = new Order();
        $manager->persist($order);

        $this->setReference(self::REFERENCE_ORDER, $order);

        $manager->flush();
    }
}