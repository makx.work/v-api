<?php declare(strict_types=1);

namespace App\Tests\Unit\Domain\Order;

use App\Domain\Order\Order;
use PHPUnit\Framework\TestCase;

class OrderTest extends TestCase
{
    public function testCreateSuccess()
    {
        $order = new Order();

        self::assertEquals(0, $order->getTotal());
    }
}