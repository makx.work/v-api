<?php declare(strict_types=1);

namespace App\Tests\Unit\Domain\Product;

use App\Domain\Product\BadProductPriceException;
use App\Domain\Product\Product;
use PHPUnit\Framework\TestCase;

class ProductTest extends TestCase
{
    public function testCreateSuccess()
    {
        $product = new Product($name = 'Product name', $price = 10);

        self::assertEquals($name, $product->getName());
        self::assertEquals($price, $product->getPrice());
    }

    public function testCreateWrongPrice()
    {
        self::expectException(BadProductPriceException::class);
        new Product($name = 'Product name', $price = -12);

        self::expectException(BadProductPriceException::class);
        new Product($name = 'Product name', $price = 200);
    }
}