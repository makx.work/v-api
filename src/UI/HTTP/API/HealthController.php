<?php

declare(strict_types=1);

namespace App\UI\HTTP\API;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;

final class HealthController
{
    /**
     * @Route("/health", name="health", methods={"GET"})
     *
     * @return JsonResponse
     */
    public function health(): JsonResponse
    {
        return new JsonResponse('OK');
    }
}
