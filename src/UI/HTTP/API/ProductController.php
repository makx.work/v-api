<?php

declare(strict_types=1);

namespace App\UI\HTTP\API;

use App\Application\Product\Create;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

final class ProductController
{
    /**
     * @Route("/product", name="product.create", methods={"POST"})
     *
     * @param Create\Command $command
     * @param Create\Handler $handler
     *
     * @return JsonResponse
     */
    public function create(Create\Command $command, Create\Handler $handler): JsonResponse
    {
        $product = $handler->handle($command);

        return new JsonResponse($product, 201);
    }
}
