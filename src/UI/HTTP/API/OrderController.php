<?php

declare(strict_types=1);

namespace App\UI\HTTP\API;

use App\Application\Order\Create;
use App\Application\Order\AddProduct;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

final class OrderController
{
    /**
     * @Route("/order", name="order.create", methods={"POST"})
     *
     * @param Create\Handler $handler
     *
     * @return JsonResponse
     */
    public function create(Create\Handler $handler): JsonResponse
    {
        $order = $handler->handle();

        return new JsonResponse($order, 201);
    }

    /**
     * @Route("/order/product/add", name="order.product.add", methods={"POST"})
     *
     * @param AddProduct\Command $command
     * @param AddProduct\Handler $handler
     *
     * @return JsonResponse
     */
    public function add(AddProduct\Command $command, AddProduct\Handler $handler): JsonResponse
    {
        $order = $handler->handle($command);

        return new JsonResponse($order, 201);
    }
}
