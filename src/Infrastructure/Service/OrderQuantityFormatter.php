<?php

declare(strict_types=1);

namespace App\Infrastructure\Service;

use App\Domain\Order\Order;
use App\Domain\OrderProduct\OrderProduct;

final class OrderQuantityFormatter
{
    /**
     * @param Order $order
     * @return array<int, array<string, int>>
     */
    public function format(Order $order): array
    {
        $data = [];
        array_map(static function (OrderProduct $orderProduct) use (&$data) {
            /** @var int $productId */
            $productId = $orderProduct->getProduct()->getId();

            /** @var array{int} $data */
            if (isset($data[$productId])) {
                $data[$productId] += 1;
            } else {
                $data[$productId] = 1;
            }
        }, $order->getOrderProducts());

        $productsQuantity = [];
        /**
         * @var int $id
         * @var int $quantity
         */
        foreach ($data as $id => $quantity) {
            $productsQuantity[] = [
                'id' => $id,
                'quantity' => $quantity
            ];
        }

        return $productsQuantity;
    }
}
