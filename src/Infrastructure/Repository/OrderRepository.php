<?php

declare(strict_types=1);

namespace App\Infrastructure\Repository;

use App\Domain\Order\Order;
use App\Domain\Order\OrderRepositoryInterface;
use App\Domain\OrderProduct\OrderProduct;
use App\Domain\Product\Product;
use Doctrine\ORM\EntityManagerInterface;

class OrderRepository implements OrderRepositoryInterface
{
    private EntityManagerInterface $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function create(Order $order): void
    {
        $this->em->persist($order);
        $this->em->flush();
    }

    /**
     * @param Order $order
     * @param Product $product
     */
    public function addProduct(Order $order, Product $product): void
    {
        $orderProduct = new OrderProduct();
        $orderProduct->setOrder($order);
        $orderProduct->setProduct($product);

        $order->addProduct($orderProduct);

        $this->em->persist($orderProduct);
        $this->em->flush();
    }

    public function find(int $id): ?Order
    {
        return $this->em->find(Order::class, $id);
    }
}
