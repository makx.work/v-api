<?php

declare(strict_types=1);

namespace App\Infrastructure\Exception;

use Doctrine\ORM\EntityNotFoundException;

final class OrderNotFoundException extends EntityNotFoundException
{

}
