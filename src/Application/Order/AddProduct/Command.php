<?php

declare(strict_types=1);

namespace App\Application\Order\AddProduct;

use App\Infrastructure\Request\API\RequestApiInterface;
use Symfony\Component\Validator\Constraints as Assert;

final class Command implements RequestApiInterface
{
    /**
     * @Assert\Type("int")
     * @Assert\NotBlank()
     */
    public int $orderId;

    /**
     * @Assert\Type("int")
     * @Assert\NotBlank()
     */
    public int $productId;
}
