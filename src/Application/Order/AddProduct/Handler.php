<?php

declare(strict_types=1);

namespace App\Application\Order\AddProduct;

use App\Domain\Order\Order;
use App\Domain\Product\ProductRepositoryInterface;
use App\Domain\Order\OrderRepositoryInterface;
use App\Infrastructure\Exception\ApiException;
use App\Infrastructure\Service\OrderQuantityFormatter;

final class Handler
{
    private ProductRepositoryInterface $productRepository;
    private OrderRepositoryInterface $orderRepository;
    private OrderQuantityFormatter $orderQuantityFormatter;

    public function __construct(
        ProductRepositoryInterface $productRepository,
        OrderRepositoryInterface $orderRepository,
        OrderQuantityFormatter $orderQuantityFormatter
    ) {
        $this->productRepository = $productRepository;
        $this->orderRepository = $orderRepository;
        $this->orderQuantityFormatter = $orderQuantityFormatter;
    }

    /**
     * @param Command $command
     * @return array<string, int|array>
     */
    public function handle(Command $command): array
    {
        $product = $this->productRepository->find($command->productId);
        $order = $this->orderRepository->find($command->orderId);

        if ($product && $order) {
            $this->orderRepository->addProduct($order, $product);

            return $this->orderFormatter($order);
        }

        throw new ApiException('Product or Order not found');
    }

    /**
     * @param Order $order
     * @return array<string, int|array>
     */
    private function orderFormatter(Order $order): array
    {
        return [
            'id' => $order->getId(),
            'total_price' => $order->getTotal(),
            'products' => $this->orderQuantityFormatter->format($order)
        ];
    }
}
