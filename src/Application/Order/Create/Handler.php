<?php

declare(strict_types=1);

namespace App\Application\Order\Create;

use App\Domain\Order\Order;
use App\Domain\Order\OrderRepositoryInterface;

final class Handler
{
    private OrderRepositoryInterface $orderRepository;

    public function __construct(OrderRepositoryInterface $orderRepository)
    {
        $this->orderRepository = $orderRepository;
    }

    public function handle(): Order
    {
        $order = new Order();
        $this->orderRepository->create($order);

        return $order;
    }
}
