<?php

declare(strict_types=1);

namespace App\Application\Product\Create;

use App\Infrastructure\Request\API\RequestApiInterface;
use Symfony\Component\Validator\Constraints as Assert;

final class Command implements RequestApiInterface
{
    /**
     * @Assert\Type("string")
     * @Assert\NotBlank()
     */
    public string $name;

    /**
     * @Assert\Type("int")
     * @Assert\NotBlank()
     *
     * @Assert\Range(
     *      min = 1,
     *      max = 100,
     *      notInRangeMessage = "Price must be between {{ min }} and {{ max }}",
     * )
     *
     */
    public int $price;
}
