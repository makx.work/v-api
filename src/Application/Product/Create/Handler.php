<?php

declare(strict_types=1);

namespace App\Application\Product\Create;

use App\Domain\Product\Product;
use App\Domain\Product\ProductRepositoryInterface;
use App\Infrastructure\Exception\ApiException;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

final class Handler
{
    private ProductRepositoryInterface $productRepository;
    private ValidatorInterface $validator;
    private SerializerInterface $serializer;

    public function __construct(
        ProductRepositoryInterface $productRepository,
        ValidatorInterface $validator,
        SerializerInterface $serializer
    ) {
        $this->productRepository = $productRepository;
        $this->validator = $validator;
        $this->serializer = $serializer;
    }

    public function handle(Command $command): Product
    {
        $product = new Product($command->name, $command->price);

        $violations = $this->validator->validate($product);

        if (\count($violations)) {
            $json = $this->serializer->serialize($violations, 'json');

            throw new ApiException($json);
        }

        $this->productRepository->create($product);

        return $product;
    }
}
