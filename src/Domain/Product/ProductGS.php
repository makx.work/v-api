<?php

declare(strict_types=1);

namespace App\Domain\Product;

use App\Domain\OrderProduct\OrderProduct;
use Doctrine\Common\Collections\ArrayCollection;

trait ProductGS
{
    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getPrice(): int
    {
        return $this->price;
    }

    public function setPrice(int $price): void
    {
        $this->price = $price;
    }

    /**
     * @return ArrayCollection<int, OrderProduct>
     */
    public function getProductOrders(): ArrayCollection
    {
        return $this->productOrders;
    }

    /**
     * @param ArrayCollection<int, OrderProduct> $productOrders
     */
    public function setProductOrders(ArrayCollection $productOrders): void
    {
        $this->productOrders = $productOrders;
    }
}
