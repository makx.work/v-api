<?php

declare(strict_types=1);

namespace App\Domain\Product;

use App\Domain\OrderProduct\OrderProduct;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity()
 * @ORM\Table()
 * @UniqueEntity(fields={"name"})
 */
class Product implements \JsonSerializable
{
    use ProductGS;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\Column(type="string", length=100, nullable=false, unique=true)
     *
     * @var string
     */
    private string $name;

    /**
     * @ORM\Column(type="integer", nullable=false)
     *
     * @var int
     */
    private int $price;

    /**
     *  @var ArrayCollection<int, OrderProduct>
     *
     * @ORM\OneToMany(targetEntity="App\Domain\OrderProduct\OrderProduct", mappedBy="product")
     */
    private $productOrders;

    public function __construct(string $name, int $price)
    {
        $this->name = $name;
        $this->productOrders = new ArrayCollection();
        $this->setPrice($price);
    }

    private function setPrice(int $price): void
    {
        if ($price > 0 && $price <= 100) {
            $this->price = $price;
        } else {
            throw new BadProductPriceException();
        }
    }

    public function jsonSerialize()
    {
        return [
           'id' => $this->getId(),
           'name' => $this->getName(),
           'price' => $this->getPrice()
        ];
    }
}
