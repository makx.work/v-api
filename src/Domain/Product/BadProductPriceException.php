<?php

declare(strict_types=1);

namespace App\Domain\Product;

class BadProductPriceException extends \InvalidArgumentException
{
}
