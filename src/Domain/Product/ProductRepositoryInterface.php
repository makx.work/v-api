<?php

declare(strict_types=1);

namespace App\Domain\Product;

interface ProductRepositoryInterface
{
    public function create(Product $product): void;
    public function find(int $id): ?Product;
}
