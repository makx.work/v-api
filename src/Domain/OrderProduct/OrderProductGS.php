<?php

declare(strict_types=1);

namespace App\Domain\OrderProduct;

use App\Domain\Order\Order;
use App\Domain\Product\Product;

trait OrderProductGS
{
    public function getId(): int
    {
        return $this->id;
    }

    public function getOrder(): Order
    {
        return $this->order;
    }

    public function setOrder(Order $order): void
    {
        $this->order = $order;
    }

    public function getProduct(): Product
    {
        return $this->product;
    }

    public function setProduct(Product $product): void
    {
        $this->product = $product;
    }
}
