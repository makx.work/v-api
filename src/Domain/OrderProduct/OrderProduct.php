<?php

declare(strict_types=1);

namespace App\Domain\OrderProduct;

use App\Domain\Order\Order;
use App\Domain\Product\Product;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table()
 */
class OrderProduct
{
    use OrderProductGS;

    /**
     * @var int
     *
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue()
     */
    private int $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Domain\Order\Order", inversedBy="orderProducts")
     */
    private Order $order;

    /**
     *  @ORM\ManyToOne(targetEntity="App\Domain\Product\Product", inversedBy="orderProducts")
     */
    private Product $product;
}
