<?php

declare(strict_types=1);

namespace App\Domain\Order;

use App\Domain\OrderProduct\OrderProduct;

trait OrderGS
{
    public function getId(): int
    {
        return $this->id;
    }

    public function getTotal(): int
    {
        return $this->total;
    }

    public function setTotal(int $total): void
    {
        $this->total = $total;
    }

    /**
     * @return OrderProduct[]
     */
    public function getOrderProducts(): array
    {
        return $this->orderProducts->toArray();
    }

    public function addProduct(OrderProduct $orderProduct): void
    {
        $this->orderProducts->add($orderProduct);
        $this->total += $orderProduct->getProduct()->getPrice();
    }

    public function removeProduct(OrderProduct $orderProduct): void
    {
        $this->orderProducts->removeElement($orderProduct);
        $this->total -= $orderProduct->getProduct()->getPrice();
    }
}
