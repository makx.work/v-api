<?php

declare(strict_types=1);

namespace App\Domain\Order;

use App\Domain\OrderProduct\OrderProduct;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="`order`")
 */
class Order implements \JsonSerializable
{
    use OrderGS;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\Column(type="integer", nullable=false)
     *
     * @var int
     */
    private int $total;

    /**
     * @var ArrayCollection<int, OrderProduct>
     *
     * @ORM\OneToMany(targetEntity="App\Domain\OrderProduct\OrderProduct", mappedBy="order")
     */
    private $orderProducts;

    public function __construct()
    {
        $this->orderProducts = new ArrayCollection();
        $this->total = 0;
    }

    public function jsonSerialize()
    {
        return [
            'id' => $this->id,
            'total' => $this->total
        ];
    }
}
