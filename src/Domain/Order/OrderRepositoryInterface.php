<?php

declare(strict_types=1);

namespace App\Domain\Order;

use App\Domain\Product\Product;

interface OrderRepositoryInterface
{
    public function create(Order $order): void;
    public function addProduct(Order $order, Product $product): void;
    public function find(int $id): ?Order;
}
